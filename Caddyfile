# it used to be that some of my domains were proxied through a CDN and dns-01
# was required. it is no longer the case and dns-01 exists so I can figure out
# how to obtain and use a wildcard certificate with caddy.
(acme_dns) {
  tls {
    dns gandi {env.GANDIV5_API_TOKEN}
  }
}

# http-01 challenge
(acme_http) {
  tls mail@kayg.org
}

# reusable snippets
# bare security headers
(security_headers) {
  # Cache-Control is a HTTP header that defines the amount of time and manner a
  # file is to be cached. https://varvy.com/pagespeed/cache-control.html
  # Also read: https://support.cloudflare.com/hc/en-us/articles/115003206852-Understanding-Origin-Cache-Control
  # Cache in intermediary as well as client caches but prefer revalidation when served
  # Cache-Control "public, no-cache"

  # Allows a site to control which features and APIs can be used in the browser.
  # https://scotthelme.co.uk/a-new-security-header-feature-policy/
  Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'self'; camera 'none'; encrypted-media 'none'; fullscreen 'self'; geolocation 'none'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; payment 'none'; picture-in-picture *; speaker 'none'; sync-xhr 'none'; usb 'none'; vr 'none'"

  # Only send Referer header to same origin. Django CSRF protection is
  # incompatible with referrer policy set to none.
  Referrer-Policy "same-origin"

  # Don't show Caddy/Gunicorn as server header.
  -Server

  # Enable HTTP Strict Transport Security (HSTS) to force clients to always
  # connect via HTTPS
  Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"

  # Prevent some browsers from MIME-sniffing a response away from the declared
  # Content-Type
  X-Content-Type-Options "nosniff"

  # Don't allow resources to load within a frame/iframe. This is handled with
  # frame-ancestors 'none' in the content security policy. But not yet supported
  # by older browsers.
  X-Frame-Options "SAMEORIGIN"

  # Enable cross-site filter (XSS) and tell browser to block detected attacks.
  X-XSS-Protection "1; mode=block"

  # Enable a strict content security policy.
  # Edit this if you need external sources on your site.
  # See: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
  Content-Security-Policy "upgrade-insecure-requests; frame-ancestors https://*.kayg.org"
}

# security headers under the header directive with CSP and http01 validation
(security_http) {
  import acme_http

  header / {
    import security_headers
  }
}

# security headers under the header directive with CSP and dns01 validation
(security) {
  import acme_dns

  header / {
    import security_headers
  }
}

# collabora
collabora.kayg.org {
  import security

  reverse_proxy http://collabora:9980
}

# cryptpad
pad.kayg.org {
  import security

  reverse_proxy http://cryptpad:3000
}

# cryptpad sandbox
pad-sandbox.kayg.org {
  import security

  reverse_proxy http://cryptpad:3001
}

# film diary
# I no longer use Ghost for hosting my film blog and have migrated
# to Wordpress completely. This block will handle redirects now.
film.kayg.org {
  handle /list-review* {
    redir https://kayg.org/film/list-review/
  }

  handle /mind-game-review* {
    redir https://kayg.org/film/mind-game-review/
  }

  handle /following-review* {
    redir https://kayg.org/film/following-review/
  }

  handle /right-now-wrong-then-review* {
    redir https://kayg.org/film/right-now-wrong-then-review/
  }

  handle /ae-dil-hai-mushkil-review* {
    redir https://kayg.org/film/ae-dil-hai-mushkil-review/
  }

  handle /dunkirk-review* {
    redir https://kayg.org/film/dunkirk-review/
  }

  handle /virgin-stripped-bare-by-her-bachelors-review* {
    redir https://kayg.org/film/virgin-stripped-bare-by-her-bachelors-review/
  }

  handle /the-power-of-kangwon-province-review* {
    redir https://kayg.org/film/the-power-of-kangwon-province-review/
  }

  handle /the-day-a-pig-fell-into-the-well-review* {
    redir https://kayg.org/film/the-day-a-pig-fell-into-the-well-review/
  }

  handle {
    redir https://kayg.org/film/
  }
}

# i no longer use gitea as I find GitLab(.com) to be a much better platform for
# my usecase. the block below handles all directions from my gitea to gitlab
# profile.
git.kayg.org {
  redir https://gitlab.com/kayg/
}

# h5ai
public.kayg.org {
  import security

  reverse_proxy http://h5ai:80
}

# jirafeau
share.kayg.org {
  import security

  reverse_proxy http://jirafeau:80
}

# landing page
www.kayg.org {
  import security

  reverse_proxy http://wordpress:80
}

# redirect www to non-www
kayg.org {
  redir https://www.kayg.org{uri}
}

# nextcloud
cloud.kayg.org {
  import security

  reverse_proxy https://nextcloud:443 {
    transport http {
      tls_insecure_skip_verify
    }
  }
}

# nitter
tweet.kayg.org {
  import security

  reverse_proxy http://nitter:8080
}

# privatebin
bin.kayg.org {
  import security

  reverse_proxy http://privatebin:8080
}

qmk.kayg.org {
  import security

  reverse_proxy http://qmk_configurator:80
}

# searx
search.kayg.org {
  import security

  reverse_proxy http://searx:8080
}

# technical diary
#
# i've moved this blog to a subpath instead for SEO reasons. the block below
# handles all redirections.
tech.kayg.org {
  handle /privacy/hello-internet* {
    redir https://kayg.org/tech/hello-internet/
  }

  handle /privacy/playing-the-game* {
    redir https://kayg.org/tech/playing-the-game/
  }

  handle /stories/the-duality-of-a-popular-content-management-system* {
    redir https://kayg.org/tech/the-duality-of-a-popular-content-management-system/
  }

  handle {
    redir https://kayg.org/tech/
  }
}

# thelounge
irc.kayg.org {
  import security

  reverse_proxy http://thelounge:9000
}

# uptime robot
status.kayg.org {
  redir https://stats.uptimerobot.com/ZZ7JvtMPzr
}

# vaultwarden
vault.kayg.org {
  import security

  # The negotiation endpoint is also proxied to Rocket
  reverse_proxy /notifications/hub/negotiate http://vaultwarden:2048

  # Notifications redirected to the websockets server
  reverse_proxy /notifications/hub http://vaultwarden:3012

  # Reverse_proxy the root directory to Rocket
  reverse_proxy http://vaultwarden:2048
}

# YOURLS
go.kayg.org {
  import security

  reverse_proxy http://yourls:80 {
    # Allow responses to be read if the request originates
    # from bin.kayg.org (shortened URL of privatebin)
    header_down Access-Control-Allow-Origin "https://bin.kayg.org"
  }
}
